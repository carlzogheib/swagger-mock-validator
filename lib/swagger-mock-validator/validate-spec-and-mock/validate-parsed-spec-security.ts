import * as _ from 'lodash';
import {ValidationResult} from '../../api-types';
import {ParsedMockInteraction} from '../mock-parser/parsed-mock';
import {result} from '../result';
import {
    ParsedSpecOperation,
    ParsedSpecSecurityRequirement,
    ParsedSpecSecurityRequirements
} from '../spec-parser/parsed-spec';

const validateQueryRequirement = (
    parsedSpecSecurityRequirement: ParsedSpecSecurityRequirement,
    parsedMockInteraction: ParsedMockInteraction
) => {
    if (!parsedMockInteraction.requestQuery[parsedSpecSecurityRequirement.credentialKey]) {
        return result.build({
            code: 'request.authorization.missing',
            message: 'Request Authorization query is missing but is required by the spec file',
            mockSegment: parsedMockInteraction,
            source: 'spec-mock-validation',
            specSegment: parsedSpecSecurityRequirement
        });
    }

    return undefined as any;
};

const validateHeaderRequirement = (
    parsedSpecSecurityRequirement: ParsedSpecSecurityRequirement,
    parsedMockInteraction: ParsedMockInteraction
) => {
    if (!parsedMockInteraction.requestHeaders[parsedSpecSecurityRequirement.credentialKey]) {
        return result.build({
            code: 'request.authorization.missing',
            message: 'Request Authorization header is missing but is required by the spec file',
            mockSegment: parsedMockInteraction,
            source: 'spec-mock-validation',
            specSegment: parsedSpecSecurityRequirement
        });
    }

    return undefined as any;
};

const validateRequirement = (
    parsedMockInteraction: ParsedMockInteraction,
    parsedSpecSecurityRequirements: ParsedSpecSecurityRequirements
): ValidationResult[] => {
    return _(parsedSpecSecurityRequirements)
        .map((parsedSpecSecurityRequirement) => {
            if (parsedSpecSecurityRequirement.credentialLocation === 'query') {
                return validateQueryRequirement(parsedSpecSecurityRequirement, parsedMockInteraction);
            }

            return validateHeaderRequirement(parsedSpecSecurityRequirement, parsedMockInteraction);
        })
        .compact()
        .value();
};

export const validateParsedSpecSecurity = (
    parsedMockInteraction: ParsedMockInteraction,
    parsedSpecOperation: ParsedSpecOperation
): ValidationResult[] => {
    const validationResultsPerRequirement = _(parsedSpecOperation.securityRequirements)
        .map((requirements) => {
            return validateRequirement(parsedMockInteraction, requirements);
        });

    const anySecurityRequirementsMet = validationResultsPerRequirement
        .some((validationResults: ValidationResult[]) => validationResults.length === 0);

    if (anySecurityRequirementsMet) {
        return [];
    }

    return validationResultsPerRequirement.first() || [];
};
